#include <cstring>
#include <iostream>
#include <fstream>
#include"mainwindow.h"
#include"card.h"
#include"MyTests.h"

using namespace std;

int gDone = 0;
int gFail = 0;


/********************************************************************************/
void NewTests()
{
	std::ofstream log("MyTestsLog.txt");
	log << "===================================================\n"
		<< "                  New Tests\n\n";
}


/********************************************************************************/
void Log_Of_MyTests(bool valid, int line, const char* file, const char* fuNname, const char* expresion)
{
	std::ofstream log("MyTestsLog.txt", std::ios::app);

	const char* fileName = strrchr(file, '\\');
	fileName += 1;

	if (valid)
	{
		log << "DONE: ";
		gDone++;
	}
	else
	{
		log << "FAIL: ";
		gFail++;
	}

	log << fileName << ": " << fuNname << ": " << line << " " << expresion << "\n";
}


/********************************************************************************/
void EndTests()
{
	std::ofstream log("MyTestsLog.txt", std::ios::app);
	log << "\n===============\n"
		<< "Total tests: " << (gDone + gFail)
		<< "\nDone tests: " << gDone
		<< "\nFail tests: " << gFail
		<< "\n===============\n"
		<< "\n                  End Tests\n"
		<< "===================================================\n"
		<< "===================================================\n\n\n";
}
/*********************************************************************************/
void test_Card_label_inf(Card_label* temp, vector<Card_label*>& inVector )
{
    DO_TEST( inVector[ inVector.size() - 1]->get_value()          == temp->get_value()           );
    DO_TEST( inVector[ inVector.size() - 1]->get_isRedCard()      == temp->get_isRedCard()       );
    DO_TEST( inVector[ inVector.size() - 1]->get_back_image()     == temp->get_back_image()      );
    DO_TEST( inVector[ inVector.size() - 1]->get_front_image()    == temp->get_front_image()     );
    DO_TEST( inVector[ inVector.size() - 1]->get_isFrontImage()   == temp->get_isFrontImage()    );
    DO_TEST( inVector[ inVector.size() - 1]->get_number_in_stack()== temp->get_number_in_stack() );
    DO_TEST( inVector[ inVector.size() - 1]->get_number_of_stack()== temp->get_number_of_stack() );
    DO_TEST( inVector[ inVector.size() - 1]->get_number_of_value()== temp->get_number_of_value() );
    DO_TEST( inVector[ inVector.size() - 1]->get_mast()           == temp->get_mast()            );
}
