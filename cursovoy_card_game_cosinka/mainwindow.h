#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "card.h"
#include "stack"
#include "QGraphicsSceneMouseEvent"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

// CONST numbers of stacks
//
/*
 * 0        - card_deck
 * 1        - card_stack
 * (2 - 5)  - final_card_stacks
 * (6 - 12) - card_game_stacks
*/
/**********************************************************************************/
class Card_label: public QLabel
{
    Q_OBJECT

    public:

    Card_label();
    ~Card_label(){};

    // setters
    //
    void     set_card_parametres         ( Card inCard);
    void     set_card_parameters         ( Card_label& inCard_label);
    void     set_card_up_stack_parameters();
    void     set_isFrontImage            ( bool inBool )   { isFrontImage    = inBool; }
    void     set_number_of_stack         (int inNumber );

    //getters
    //
    QPixmap* get_back_image()                   const { return back_image; };
    QPixmap* get_back_image_of_card_label()     const { return back_image; }
    QLabel*  get_empty_label_of_final_stack()   const;
    QPixmap* get_front_image()                  const { return front_image; };
    QPixmap* get_front_image_of_card_label()    const { return front_image; }
    bool     get_isFrontImage()                 const { return isFrontImage; }
    bool     get_isRedCard()                    const { return isRedCard; }
    QString  get_mast()                         const { return mast; }
    int      get_number_in_stack()              const { return number_in_stack; }
    int      get_number_of_stack()              const { return number_of_stack; }
    int      get_number_of_value()              const;
    QString  get_value()                        const { return value; }


    // mouse events
    //
    void     mouseMoveEvent   (QMouseEvent* event);
    void     mousePressEvent  (QMouseEvent *event);
    void     mouseReleaseEvent(QMouseEvent *event);

    void     move_group_of_cards( Card_label* inCard, QMouseEvent* inEvent );

    private:

    // this functions use in set_number_of_stack()
    //
     void    set_number_in_stack();
     void    drop_number_in_stack();

     void    get_mouse_positition(int &number_of_value_of_the_last_card_in_layout_where_i_locate_a_card, int &number_of_stack_where_I_locate_card, QMouseEvent *event);

     // locating of 1 card
     //
     void locate_card_of_final_stack_in_down_v_layout(
             int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
             int number_of_stack_where_I_locate_card,
             int number_of_stack_where_I_take_card,
             Card_label* inCard );

     void locate_card_of_game_vector_in_down_v_layout(
             int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
             int number_of_stack_where_I_locate_card,
             int number_of_stack_where_I_take_card,
             Card_label* inCard );

     void locate_card_of_game_vector_in_up_v_layout(      // here I locate this_card over other cards
             int number_of_stack_where_I_locate_card,
             int number_of_stack_where_I_take_card,
             Card_label* inCard );

     void locate_card_of_stack_in_down_v_layout(
             int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
             int number_of_stack_where_I_locate_card,
             Card_label* inCard );

     void locate_card_of_stack_in_up_v_layout(            // here I locate this_card over other cards
             int number_of_stack_where_I_locate_card,
             Card_label* inCard );

    // locating group of card
    //
     void locate_group_of_cards_in_down_v_layout(
             int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
             int number_of_stack_where_I_locate_card,
             int number_of_stack_where_I_take_card,
             Card_label* inCard
             );

protected:

        bool     isRedCard;
        bool     isFrontImage;

        QString  mast;
        QString  value;

        QPixmap* front_image;
        QPixmap* back_image;

        int      number_of_stack = -1; // parameter, which shows his location( in which stack )
        int      number_in_stack;      // parameter, which shows his location( in which place in stack )

        QPoint   point_of_mouse_press;

}; // Card_label

/**********************************************************************************/
class MainWindow : public QMainWindow
{    
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void locate_cards_on_up_h_layout();
    void locate_cards_on_down_v_layout( int inLength_of_layout );

    void init_deck();
    void init_layouts();
    void init_cards();
    void init_images();
    void init_sound_for_cards();

    void set_next_card_to_card_stack( Card_label* inCard_label );
    void set_down_v_layouts_empty_labels();

    void test_mainwindow_inf();

private slots:
    void get_card_from_card_deck();

private:
    Ui::MainWindow *ui;
    Card*          deck;

    QVBoxLayout *  main_v_layout;
    QHBoxLayout *  down_h_layout;

    QPixmap* image_empty;
    QPixmap* image_back_of_card;

// elements in up_h_layout
//
    QPushButton* next_card_btn;
    QLabel*      razdelitel;

}; // MainWindow

// Tests functions
//
/**********************************************************************************/
void test_card_inf( Card* deck );
void test_Card_label_inf( Card_label* temp, vector<Card_label*>& inVector );

void test_global_inf();

#endif // MAINWINDOW_H
