#ifndef CARD_H
#define CARD_H

#include "string"
#include "QtWidgets"

using namespace std;

/**********************************************************************************/
class Card
{

    public://///////////////////////////////////////////////////////////////////

        Card();

        ~Card(){};

        QString  get_mast() const{ return mast; };
        QString  get_value() const{ return value; };
        QPixmap* get_front_image() const { return front_image; };
        QPixmap* get_back_image() const { return back_image; };
        bool     get_isRedCard()const { return isRedCard; };



    protected://////////////////////////////////////////////////////////////////

        QString  mast;
        QString  value;
        bool     isRedCard;
        QPixmap* front_image;
        QPixmap* back_image;

}; // Card

    Card* mix_deck( Card inDeck[] );

#endif // CARD_LABEL_H
