#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "card.h"
#include <QMediaPlayer>
#include"MyTests.h"

// constants
// card`s constants
const int   down_stack_card_height  = 40;  // px
const int   card_width              = 152; // px
const int   card_height             = 220; // px
const int   amount_of_cards_in_deck = 52;
const QSize btn_icon_size(152,220);

// game`s constants
// number of cards on top left corner
const int card_deck_number = 0;
const int card_stack_number = 1;

// number of cards on top right corner
const int final_stack1_number = 2;
const int final_stack2_number = 3;
const int final_stack3_number = 4;
const int final_stack4_number = 5;

// number of cards on a bottom half of window
const int game_vector1_number  = 6;
const int game_vector2_number  = 7;
const int game_vector3_number  = 8;
const int game_vector4_number  = 9;
const int game_vector5_number  = 10;
const int game_vector6_number  = 11;
const int game_vector7_number  = 12;

// amount of cards of the same mast
const int size_of_final_stacks = 13;

const QString values[]
{
    "A",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "V",
    "Q",
    "K"
};

// end constants
//

// global parametres
// amount of cards on layouts
int number_of_card_in_deck = 0;
int number_of_card_in_stack = 0;

int final_card_stack1_number = 0;
int final_card_stack2_number = 0;
int final_card_stack3_number = 0;
int final_card_stack4_number = 0;

int card_game_vector1_number = 0;
int card_game_vector2_number = 0;
int card_game_vector3_number = 0;
int card_game_vector4_number = 0;
int card_game_vector5_number = 0;
int card_game_vector6_number = 0;
int card_game_vector7_number = 0;

// up_h_layout
vector<Card_label*> cards_deck;
vector<Card_label*> card_stack;

// up_h_layout and up_v_layout
vector<Card_label*> final_stack[4];

// down_v_layout
vector<Card_label*> game_vectors[7];

// for view of empty vectors
// if game_vector[i] is empty, that other game_vectors
// don`t must  remove
QLabel* empty_label1;
QLabel* empty_label2;
QLabel* empty_label3;
QLabel* empty_label4;
QLabel* empty_label5;
QLabel* empty_label6;
QLabel* empty_label7;

// if final_stack[i] is empty, that other final_stacks
// don`t must  remove
QLabel* final_empty_label1;
QLabel* final_empty_label2;
QLabel* final_empty_label3;
QLabel* final_empty_label4;

// if card_stack is empty, that other elements
// on up_h_layout don`t must remove
QLabel*      top_stack_card;

QHBoxLayout* up_h_layout;
QVBoxLayout* up_v_layouts;

QVBoxLayout* down_v_layout;

// for identification of card groups
//
int count_cards_in_group;
int size_of_visible_group_of_cards;

// for playing of sound, when I let off( отпускать ) mouse`s button
//
QMediaPlayer* sound_of_card;
/*********************************************************************************/
//It uses for playing of sound
//
void play_sound()
{
    sound_of_card->play();
    QTimer::singleShot(500, sound_of_card, SLOT(stop() ));
    sound_of_card->setPosition(4);
}
/*********************************************************************************/
void check_win()
{
    int koef_of_victory = 0;

    if(  final_card_stack1_number == size_of_final_stacks ) koef_of_victory++;
    if(  final_card_stack2_number == size_of_final_stacks ) koef_of_victory++;
    if(  final_card_stack3_number == size_of_final_stacks ) koef_of_victory++;
    if(  final_card_stack4_number == size_of_final_stacks ) koef_of_victory++;

    if( koef_of_victory == 4)
    {
        QLabel* congratulations_label = new QLabel("You win!");

        congratulations_label->setGeometry(
                    QApplication::desktop()->width() / 3,
                    QApplication::desktop()->height() / 3
                    , 400, 300); // width and height of window

        congratulations_label->setStyleSheet(
                    "font-size : 100px;"
                    "color: white;"
                    "background-color: green;"
                    "text-align: center;"
                    );

        congratulations_label->show();
    }

}

/*******************************************************************/
void get_count_of_card_in_group( Card_label* inCard )
{
    count_cards_in_group = 1;
    size_of_visible_group_of_cards = 0;

    int number_of_stack = inCard->get_number_of_stack();
    int number_in_stack = inCard->get_number_in_stack();
    int size_of_stack = game_vectors[ number_of_stack - game_vector1_number ].size();


    for( int i = number_in_stack; i < size_of_stack ; i++ )
    {
        if( game_vectors[ number_of_stack - game_vector1_number ][ i ]->get_isFrontImage() )
            size_of_visible_group_of_cards++;
    }


    if( number_of_stack >= game_vector1_number && number_of_stack <= game_vector7_number )
    {
        if( size_of_stack - 1 != number_in_stack )
        {
            for( int i = number_in_stack; i < size_of_stack - 1; i++ )
            {
                if( (game_vectors[ number_of_stack - game_vector1_number ][ i ]->get_number_of_value()
                        ==
                     game_vectors[ number_of_stack - game_vector1_number ][ i + 1 ]->get_number_of_value() + 1)
                                &&
                    (game_vectors[ number_of_stack - game_vector1_number ][ i ]->get_isRedCard()
                        !=
                     game_vectors[ number_of_stack - game_vector1_number ][ i + 1 ]->get_isRedCard() )
                  )
                {
                     count_cards_in_group++;
                }
                else
                    break;

            }
        }
    }

}
/*******************************************************************/
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    NewTests();


    init_images();
    init_sound_for_cards();
    init_deck();
    init_layouts();
    init_cards();

    test_mainwindow_inf();
    test_global_inf();

    EndTests();

}

/*******************************************************************/
void MainWindow::locate_cards_on_up_h_layout()
{
        next_card_btn  = new QPushButton();
        top_stack_card = new QLabel();
        razdelitel     = new QLabel(); // for better location of other elements on up_h_layout

        final_empty_label1 = new QLabel();
        final_empty_label2 = new QLabel();
        final_empty_label3 = new QLabel();
        final_empty_label4 = new QLabel();

        next_card_btn->setIcon(* image_back_of_card);
        next_card_btn->setIconSize( btn_icon_size );
        next_card_btn->setMinimumSize( btn_icon_size );
        next_card_btn->setMaximumSize( btn_icon_size );
        connect(next_card_btn, SIGNAL( clicked() ), this, SLOT(get_card_from_card_deck() ) );


        top_stack_card->setPixmap(* image_empty);

        up_h_layout->addWidget(next_card_btn);
        up_h_layout->setAlignment(next_card_btn, Qt::AlignHCenter | Qt::AlignTop);

        up_h_layout->addWidget(top_stack_card);
        up_h_layout->setAlignment(top_stack_card, Qt::AlignHCenter | Qt::AlignTop);

        razdelitel->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
        razdelitel->setMinimumWidth(card_width);
        razdelitel->setMaximumWidth(card_width);
        razdelitel->setMinimumHeight(card_height);
        razdelitel->setMaximumHeight(card_height);

        up_h_layout->addWidget(razdelitel);
        up_h_layout->setAlignment(razdelitel, Qt::AlignHCenter);

        for (int i = 0; i < 4; i++)
        {
            up_v_layouts[i].setSpacing(0);

            up_h_layout->addLayout(up_v_layouts + i);
        }

        up_v_layouts[0].addWidget( final_empty_label1 );
        final_empty_label1->setPixmap( *image_empty );
        up_v_layouts[0].setAlignment( final_empty_label1, Qt::AlignHCenter| Qt::AlignTop);

        up_v_layouts[1].addWidget( final_empty_label2 );
        final_empty_label2->setPixmap( *image_empty );
        up_v_layouts[1].setAlignment( final_empty_label2, Qt::AlignHCenter| Qt::AlignTop);

        up_v_layouts[2].addWidget( final_empty_label3 );
        final_empty_label3->setPixmap( *image_empty );
        up_v_layouts[2].setAlignment( final_empty_label3, Qt::AlignHCenter| Qt::AlignTop);

        up_v_layouts[3].addWidget( final_empty_label4 );
        final_empty_label4->setPixmap( *image_empty );
        up_v_layouts[3].setAlignment( final_empty_label4, Qt::AlignHCenter| Qt::AlignTop);
}

/*******************************************************************/
void MainWindow::locate_cards_on_down_v_layout( int inLength_of_layout )
{

    for( int i = 0; i < inLength_of_layout; i++ )
    {
        int size_of_deck = cards_deck.size();

        if( i == (inLength_of_layout - 1) )
            cards_deck[ size_of_deck - 1 ]->set_card_up_stack_parameters();


        down_v_layout[ inLength_of_layout - 1 ].addWidget( cards_deck[ size_of_deck - 1] );
        down_v_layout[ inLength_of_layout - 1 ].setAlignment( cards_deck[ size_of_deck - 1], Qt::AlignHCenter | Qt::AlignTop);

        game_vectors[ inLength_of_layout - 1 ].push_back( cards_deck[ size_of_deck - 1] );

        cards_deck[ size_of_deck - 1]->set_number_of_stack( inLength_of_layout + 5 );

        cards_deck.pop_back();
    }

    down_v_layout[ inLength_of_layout - 1 ].setAlignment(Qt::AlignTop);
}

/*******************************************************************/
void MainWindow::init_deck()
{
    deck = new Card[ amount_of_cards_in_deck ];

    test_card_inf( deck );

    deck = mix_deck( deck);

    for( int i =0 ; i < amount_of_cards_in_deck; i++ )
    {
        Card_label* temp = new Card_label();
        temp->set_card_parametres( deck[i] );

        cards_deck.push_back(temp);
        cards_deck[i]->setPixmap( *cards_deck[i]->get_back_image() );
        cards_deck[i]->set_isFrontImage( false );
        cards_deck[i]->set_number_of_stack( card_deck_number );

        test_Card_label_inf( temp, cards_deck );
    }
}

/*******************************************************************/
void MainWindow::init_layouts()
{
    main_v_layout = new QVBoxLayout;
    up_h_layout = new QHBoxLayout;
    down_h_layout = new QHBoxLayout;
    down_v_layout = new QVBoxLayout[7];
    up_v_layouts = new QVBoxLayout[4];

    main_v_layout->setSpacing(0);
    main_v_layout->setMargin(0);
    main_v_layout->setContentsMargins(4, 0, 4, 0);

    set_down_v_layouts_empty_labels();

    for (int i = 0; i < 7; i++)
    {
        down_v_layout[i].setSpacing(0);
        down_v_layout[i].setMargin(20);

        down_h_layout->addLayout(down_v_layout + i);
    }

    main_v_layout->addLayout( up_h_layout);
    main_v_layout->addLayout( down_h_layout);

    ui->centralwidget->setLayout( main_v_layout);

}

/*******************************************************************/
void MainWindow::init_cards()
{
    locate_cards_on_up_h_layout();

    locate_cards_on_down_v_layout(1);
    locate_cards_on_down_v_layout(2);
    locate_cards_on_down_v_layout(3);
    locate_cards_on_down_v_layout(4);
    locate_cards_on_down_v_layout(5);
    locate_cards_on_down_v_layout(6);
    locate_cards_on_down_v_layout(7);
}

/*******************************************************************/
void MainWindow::init_images()
{
    image_empty = new QPixmap( ":/resources/empty.png" );
    image_back_of_card = new QPixmap(":/resources/back.png");
}

/*********************************************************************************/
void MainWindow::init_sound_for_cards()
{
int count_of_sleshey = 0; // for clearing of extra text
QTemporaryDir* c = new QTemporaryDir("card_sound.mp3");

QString sound_file_path = "/cursovoy_card_game_cosinka/resources/card_sound.mp3";
QString sound_file_Dirpath = c->path() ;

// clear extra text of path of file
//
while( count_of_sleshey <= 1 )
{
    if(sound_file_Dirpath.back() == '/')
        count_of_sleshey++;

    sound_file_Dirpath.resize(sound_file_Dirpath.size() - 1);
}
    sound_of_card = new QMediaPlayer();
    sound_of_card->setMedia( QUrl::fromLocalFile(
sound_file_Dirpath + sound_file_path) ); // need to input full way of mp3-file
    sound_of_card->setVolume( 100 );
    sound_of_card->setPosition(4);
}

/*******************************************************************/
// work, where I draw a card
//
void MainWindow::set_next_card_to_card_stack(Card_label* inCard_label)
{
    up_h_layout->insertWidget(1, inCard_label);
    up_h_layout->setAlignment( inCard_label, Qt::AlignTop | Qt::AlignHCenter );
}

/*******************************************************************/
void MainWindow::set_down_v_layouts_empty_labels()
{
    empty_label1 = new QLabel();
    empty_label2 = new QLabel();
    empty_label3 = new QLabel();
    empty_label4 = new QLabel();
    empty_label5 = new QLabel();
    empty_label6 = new QLabel();
    empty_label7 = new QLabel();

    down_v_layout[ 0 ].addWidget( empty_label1 );
    down_v_layout[ 1 ].addWidget( empty_label2 );
    down_v_layout[ 2 ].addWidget( empty_label3 );
    down_v_layout[ 3 ].addWidget( empty_label4 );
    down_v_layout[ 4 ].addWidget( empty_label5 );
    down_v_layout[ 5 ].addWidget( empty_label6 );
    down_v_layout[ 6 ].addWidget( empty_label7 );

}

/*********************************************************************************/
// work, where I draw a card
//
void MainWindow::get_card_from_card_deck()
{
    play_sound();

    if( cards_deck.empty() )
    {

        while( card_stack.size() > 0 )
        {
            Card_label* temp = new Card_label();
            temp->set_card_parameters( *card_stack[ card_stack.size() - 1] );

            delete card_stack[ card_stack.size() - 1];

            cards_deck.push_back( temp );
            cards_deck[ cards_deck.size() - 1 ]->set_number_of_stack( card_deck_number );
            card_stack.pop_back();

        }

        if( cards_deck.size() > 0)
        {
            delete top_stack_card;
            top_stack_card = new QLabel();

            top_stack_card->setPixmap( *image_empty );
            up_h_layout->insertWidget(1, top_stack_card);
            up_h_layout->setAlignment( top_stack_card, Qt::AlignTop | Qt::AlignHCenter );

            next_card_btn->setIcon( *image_back_of_card );
        }
    }
    else
    {

        if( card_stack.size() > 0)        
            up_h_layout->removeWidget( card_stack[ card_stack.size() - 1] );
        else
            up_h_layout->removeWidget( top_stack_card );

        card_stack.push_back( cards_deck[ cards_deck.size() - 1 ] );
        card_stack[ card_stack.size() - 1]->set_number_of_stack( card_stack_number );
        card_stack[ card_stack.size() - 1]->set_card_up_stack_parameters();
        set_next_card_to_card_stack( card_stack[ card_stack.size() - 1] );

        cards_deck.pop_back();

        if(cards_deck.size() == 0)
            next_card_btn->setIcon( *image_empty );
    }

}
/*********************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
}

// class Card_label methods
//
/*********************************************************************************/
Card_label::Card_label()
{
    setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    setMinimumWidth(card_width);
    setMaximumWidth(card_width);
    setMinimumHeight(down_stack_card_height);
    setMaximumHeight(down_stack_card_height);
}


/*********************************************************************************/
// for getting information of Card to Card_label
//
void Card_label::set_card_parametres(Card inCard)
{
    mast        = inCard.get_mast();
    value       = inCard.get_value();
    front_image = inCard.get_front_image();
    back_image  = inCard.get_back_image();
    isRedCard   = inCard.get_isRedCard();
}

/*********************************************************************************/
// for re-creating of Card_label:
//   1. When I put cards from stack in deck
//   2.  When I put card to final_stacks
//
void Card_label::set_card_parameters(Card_label &inCard_label)
{
    mast            = inCard_label.get_mast();
    value           = inCard_label.get_value();
    front_image     = inCard_label.get_front_image();
    back_image      = inCard_label.get_back_image();
    isRedCard       = inCard_label.get_isRedCard();
    isFrontImage    = inCard_label.get_isFrontImage();
    number_of_stack = inCard_label.get_number_of_stack();
    number_in_stack = inCard_label.get_number_in_stack();
}

/*********************************************************************************/
// work. when I locate card on other layout
//
void Card_label::set_number_of_stack(int inNumber)
{
    if( number_of_stack == -1) // if card don`t has number_of_stack
    {
        number_of_stack = inNumber;
        set_number_in_stack();
    }
    else
    {
        drop_number_in_stack();
        number_of_stack = inNumber;
        set_number_in_stack();
    }
}

/*********************************************************************************/
// It uses, when I draw a card
//
void Card_label::set_card_up_stack_parameters()
{
    setPixmap(* get_front_image() );
    set_isFrontImage( true );
    setMinimumHeight(card_height);
    setMaximumHeight(card_height);
    setAlignment( Qt::AlignTop);
}

/*********************************************************************************/
void Card_label::set_number_in_stack()
{
    switch ( number_of_stack )
    {
        case card_deck_number:
            number_in_stack = number_of_card_in_deck++;
            break;

        case card_stack_number:
            number_in_stack = number_of_card_in_stack++;
            break;

        case final_stack1_number:
            number_in_stack = final_card_stack1_number++;
            break;

        case final_stack2_number:
            number_in_stack = final_card_stack2_number++;
            break;
        case final_stack3_number:
            number_in_stack = final_card_stack3_number++;
            break;

        case final_stack4_number:
            number_in_stack = final_card_stack4_number++;
            break;

        case game_vector1_number:
            number_in_stack = card_game_vector1_number++;
            break;

        case game_vector2_number:
            number_in_stack = card_game_vector2_number++;
            break;

        case game_vector3_number:
            number_in_stack = card_game_vector3_number++;
            break;

        case game_vector4_number:
            number_in_stack = card_game_vector4_number++;
            break;

        case game_vector5_number:
            number_in_stack = card_game_vector5_number++;
            break;

        case game_vector6_number:
            number_in_stack = card_game_vector6_number++;
            break;

        case game_vector7_number:
            number_in_stack = card_game_vector7_number++;
            break;
    }
}

/*********************************************************************************/
void Card_label::drop_number_in_stack()
{
    switch ( number_of_stack )
    {
        case card_deck_number:
            number_of_card_in_deck--;
            break;

        case card_stack_number:
            number_of_card_in_stack--;
            break;

        case final_stack1_number:
            final_card_stack1_number--;
            break;

        case final_stack2_number:
            final_card_stack2_number--;
            break;
        case final_stack3_number:
            final_card_stack3_number--;
            break;

        case final_stack4_number:
            final_card_stack4_number--;
            break;

        case game_vector1_number:
            card_game_vector1_number--;
            break;

        case game_vector2_number:
            card_game_vector2_number--;
            break;

        case game_vector3_number:
            card_game_vector3_number--;
            break;

        case game_vector4_number:
            card_game_vector4_number--;
            break;

        case game_vector5_number:
           card_game_vector5_number--;
            break;

        case game_vector6_number:
            card_game_vector6_number--;
            break;

        case game_vector7_number:
           card_game_vector7_number--;
            break;
    }
}

/*********************************************************************************/
// It uses, where I check values of cards, what I locate on other place ( final_stack and game_vector )
//
int Card_label::get_number_of_value() const
{
    //QString a = this->;
    for( int i = 0; i< 13; i++)
    {
        if( get_value() == values[i])
            return i;
    }
    return -1;

}

/*********************************************************************************/
// if final_stack[i] is empty. then other final_stacks don`t must relocating
//
QLabel *Card_label::get_empty_label_of_final_stack() const
{
    switch( number_of_stack )
    {
        case final_stack1_number:
            return final_empty_label1;
        break;

        case final_stack2_number:
            return final_empty_label2;
        break;

        case final_stack3_number:
            return final_empty_label3;
        break;

        case final_stack4_number:
            return final_empty_label4;
        break;
    }

    return nullptr;
}

/*********************************************************************************/
void Card_label::mousePressEvent(QMouseEvent *event)
{
    point_of_mouse_press = event->pos();

    if( number_of_stack >= game_vector1_number && number_of_stack <= game_vector7_number  )
        get_count_of_card_in_group( this );
}

/*********************************************************************************/
void Card_label::mouseMoveEvent(QMouseEvent *event)
{
    if( isFrontImage == true )       
    {
        int number_in_stack = get_number_in_stack();

        if( number_of_stack >= game_vector1_number && number_of_stack <= game_vector7_number && count_cards_in_group > 1)
            move_group_of_cards( this, event );
        else
            move( mapToParent( event->pos() - point_of_mouse_press )  );


        if( get_number_of_stack() >= game_vector1_number && get_number_of_stack() <= game_vector7_number )
            if(number_in_stack > 0)
                {
                    game_vectors[ get_number_of_stack() - game_vector1_number ][ number_in_stack - 1 ]->setMaximumHeight( card_height );
                    game_vectors[ get_number_of_stack() - game_vector1_number ][ number_in_stack - 1 ]->setMinimumHeight( card_height );
                }

    }
}

/*********************************************************************************/
void Card_label::mouseReleaseEvent(QMouseEvent *event)
{
    play_sound();
    int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card;
    int number_of_stack_where_I_locate_card = 1; // number of card stack
    int number_of_stack_where_I_take_card = get_number_of_stack();

    get_mouse_positition(
       number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
       number_of_stack_where_I_locate_card,
       event );

    if( isFrontImage == true )
    {
        // card_stack card
        //
        if( number_of_stack_where_I_take_card == card_stack_number )
            if( number_of_stack_where_I_locate_card == number_of_stack_where_I_take_card )
                up_h_layout->setAlignment( card_stack[ card_stack.size() - 1], Qt::AlignHCenter | Qt::AlignTop );            
            else
            {
                if( number_of_stack_where_I_locate_card >= game_vector1_number && number_of_stack_where_I_locate_card <= game_vector7_number )
                    locate_card_of_stack_in_down_v_layout(
                                number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
                                number_of_stack_where_I_locate_card,
                                this );

                if( number_of_stack_where_I_locate_card >= final_stack1_number && number_of_stack_where_I_locate_card <= final_stack4_number )
                    locate_card_of_stack_in_up_v_layout(
                                number_of_stack_where_I_locate_card,
                                this );
            }

        // work with final_stacks
        //
            if( number_of_stack_where_I_take_card >= final_stack1_number && number_of_stack_where_I_take_card <= final_stack4_number )
            {
                if( number_of_stack_where_I_locate_card >= game_vector1_number && number_of_stack_where_I_locate_card <= game_vector7_number )
                locate_card_of_final_stack_in_down_v_layout(
                        number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
                        number_of_stack_where_I_locate_card,
                        number_of_stack_where_I_take_card,
                        this );
                else
                {
                    up_v_layouts[number_of_stack_where_I_take_card - final_stack1_number].setAlignment( final_stack[ number_of_stack_where_I_take_card - final_stack1_number][ number_in_stack ], Qt::AlignTop | Qt::AlignCenter );
                }
            }

        // game_vector card
        //
        if( number_of_stack_where_I_take_card >= game_vector1_number && number_of_stack_where_I_take_card <= game_vector7_number )
        {
            if( number_of_stack_where_I_locate_card >= game_vector1_number && number_of_stack_where_I_locate_card <= game_vector7_number )   
            {
                if(  count_cards_in_group == size_of_visible_group_of_cards && count_cards_in_group != 1)
                {
                    locate_group_of_cards_in_down_v_layout(
                                number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
                                number_of_stack_where_I_locate_card,
                                number_of_stack_where_I_take_card,
                                this);
                }
                else
                {

                     locate_card_of_game_vector_in_down_v_layout(
                                 number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
                                 number_of_stack_where_I_locate_card,
                                 number_of_stack_where_I_take_card,
                                 this );
                }
            }
            else
            {
                if( count_cards_in_group == 1)
                {
                    if( number_of_stack_where_I_locate_card >= final_stack1_number && number_of_stack_where_I_locate_card <= final_stack4_number )
                    {
                        locate_card_of_game_vector_in_up_v_layout(
                                    number_of_stack_where_I_locate_card,
                                    number_of_stack_where_I_take_card,
                                    this );
                    }
                    else
                    {
                        if( get_number_in_stack() > 0)
                        {
                            down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
                            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMaximumHeight( down_stack_card_height );
                            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMinimumHeight( down_stack_card_height );
                        }
                        else
                            down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
                    }
                }
                else
                {
                    if( get_number_in_stack() > 0)
                    {
                        down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
                        game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMaximumHeight( down_stack_card_height );
                        game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMinimumHeight( down_stack_card_height );
                    }
                    else
                        down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);

                }


            }


        }
    }

    check_win();
}

/*********************************************************************************/
// It uses in mouseMoveEvent()
//
void Card_label::move_group_of_cards( Card_label* inCard, QMouseEvent* inEvent)
{
    QPoint pos_of_cards = inEvent->pos();

    for( int i = 0; i < count_cards_in_group; i++)
    {
        QPoint* pos_of_card = new QPoint(0, i * 40); // every card, what over this* are located downer on 40px

        const QPoint* temp = new QPoint(pos_of_cards.x() - point_of_mouse_press.x(),                      // X - position of every CARDi
                                        pos_of_cards.y() + pos_of_card->y() - point_of_mouse_press.y() ); // Y - position of every CARDi

        game_vectors[ inCard->get_number_of_stack() - game_vector1_number ][ inCard->get_number_in_stack() + i]->move( mapToParent( *temp ) );
    }
}

/*********************************************************************************/
void Card_label::get_mouse_positition(
        int &number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
        int &number_of_stack_where_I_locate_card,
        QMouseEvent *event )
{
    for( int i = 0; i < 4; i++)
    {
        if( up_v_layouts[i].geometry().contains( mapToParent( event->pos() ) ) )
        {
            if( final_stack[ i ].size() > 0)
             {
                number_of_value_of_the_last_card_in_layout_where_i_locate_a_card = final_stack[ i ][final_stack[ i ].size() - 1 ]->get_number_of_value();
                number_of_stack_where_I_locate_card = final_stack[ i ][final_stack[ i ].size() - 1 ]->get_number_of_stack();
            }
            else
            {
                number_of_value_of_the_last_card_in_layout_where_i_locate_a_card = -1;
                number_of_stack_where_I_locate_card = final_stack1_number + i;
            }

            break;
        }

    }

    for( int i = 0; i < 7; i++)
    {
        if( down_v_layout[i].geometry().contains( mapToParent( event->pos() ) ) )
        {
            if( game_vectors[i].size() > 0)
             {
                number_of_value_of_the_last_card_in_layout_where_i_locate_a_card = game_vectors[i][ game_vectors[i].size() - 1 ]->get_number_of_value();
                number_of_stack_where_I_locate_card = game_vectors[i][ game_vectors[i].size() - 1 ]->get_number_of_stack();
            }
            else
            {
                number_of_value_of_the_last_card_in_layout_where_i_locate_a_card = -1;
                number_of_stack_where_I_locate_card = game_vector1_number + i;
            }

            break;
        }
    }
}

/*********************************************************************************/
void Card_label::locate_card_of_stack_in_up_v_layout(
        int number_of_stack_where_I_locate_card,
        Card_label* inCard )
{
    if( final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() > 0)
    {
        if( (
            ( inCard->get_number_of_value() - 1) == final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ][ final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() - 1]->get_number_of_value() )
            &&
            ( inCard->get_mast() == final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ][ final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() - 1]->get_mast() ) )
        {

            // re- creating of card and locating card over cards
            //
            Card_label* temp = new Card_label();
            temp->set_card_parameters( *card_stack[ card_stack.size() - 1 ] );
            delete card_stack[ card_stack.size() - 1 ];
            card_stack.pop_back();
            temp->setMaximumHeight( card_height );
            temp->setMinimumHeight( card_height );
            temp->setPixmap( *temp->get_front_image() );
            card_stack.push_back( temp );

            up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].removeWidget(
                        final_stack[number_of_stack_where_I_locate_card - final_stack1_number][final_stack[number_of_stack_where_I_locate_card - final_stack1_number].size() - 1] );

            final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].push_back( card_stack[ card_stack.size() - 1 ] );

            if( get_number_in_stack() == 0 ) // !!!!!!!
            {
                up_h_layout->insertWidget(1, top_stack_card);
                up_h_layout->setAlignment(top_stack_card, Qt::AlignHCenter | Qt::AlignTop);
            }
            else
            {
                up_h_layout->insertWidget(1, card_stack[ card_stack.size() - 2]);
                up_h_layout->setAlignment(card_stack[ card_stack.size() - 2], Qt::AlignHCenter | Qt::AlignTop);
            }

            // for exception 2 same cards

            card_stack.pop_back();

            temp->set_number_of_stack( number_of_stack_where_I_locate_card);

            up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].addWidget(
                        final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ][ final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() - 1] );

            up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].setAlignment(
                        final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ][ final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() - 1], Qt::AlignHCenter | Qt::AlignTop);


        }
        else
        {
            up_h_layout->setAlignment(card_stack[ card_stack.size() - 1], Qt::AlignHCenter | Qt::AlignTop);
        }
    }
    else
    {
        if( inCard->get_number_of_value() == 0)
        {
                final_stack[number_of_stack_where_I_locate_card - final_stack1_number].push_back( card_stack[ card_stack.size() - 1 ] );

                if( get_number_in_stack() == 0 )
                {
                    up_h_layout->insertWidget(1, top_stack_card);
                    up_h_layout->setAlignment(top_stack_card, Qt::AlignHCenter | Qt::AlignTop);
                }
                else
                {
                    up_h_layout->insertWidget(1, card_stack[ card_stack.size() - 2]);
                    up_h_layout->setAlignment(card_stack[ card_stack.size() - 2], Qt::AlignHCenter | Qt::AlignTop);
                }

                set_number_of_stack( number_of_stack_where_I_locate_card);
                card_stack.pop_back();

                up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].removeWidget(
                            final_stack[number_of_stack_where_I_locate_card - final_stack1_number][final_stack[number_of_stack_where_I_locate_card - final_stack1_number].size() - 1]->get_empty_label_of_final_stack() );

                up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].addWidget(
                            final_stack[number_of_stack_where_I_locate_card - final_stack1_number][ final_stack[number_of_stack_where_I_locate_card - final_stack1_number].size() - 1] );

                up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].setAlignment(
                            final_stack[number_of_stack_where_I_locate_card - final_stack1_number][ final_stack[number_of_stack_where_I_locate_card - final_stack1_number].size() - 1], Qt::AlignHCenter | Qt::AlignTop);
        }
        else
        {
            up_h_layout->insertWidget(1, card_stack[ card_stack.size() - 1]);
            up_h_layout->setAlignment(card_stack[ card_stack.size() - 1], Qt::AlignHCenter | Qt::AlignTop);
        }
    }
}
/*********************************************************************************/
void Card_label::locate_card_of_stack_in_down_v_layout(
        int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
        int number_of_stack_where_I_locate_card,
        Card_label* inCard )
{
    //  check a parameter isRedCard. This value must be opposite
    //
      if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0)
      {
            if( isRedCard == game_vectors[number_of_stack_where_I_locate_card - game_vector1_number][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->isRedCard )
            {
                up_h_layout->setAlignment(card_stack[ card_stack.size() - 1], Qt::AlignHCenter | Qt::AlignTop);
            }
            else
            {
                if(  inCard->get_number_of_value() == ( number_of_value_of_the_last_card_in_layout_where_i_locate_a_card - 1 ) )
                {
                    down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number ].addWidget( this );

                    if( get_number_in_stack() == 0 )
                    {
                        up_h_layout->insertWidget(1, top_stack_card);
                        up_h_layout->setAlignment(top_stack_card, Qt::AlignHCenter | Qt::AlignTop);
                    }
                    else
                    {
                        up_h_layout->insertWidget(1, card_stack[ card_stack.size() - 2]);
                        up_h_layout->setAlignment(card_stack[ card_stack.size() - 2], Qt::AlignHCenter | Qt::AlignTop);
                    }

                    // where i locate a card_label
                    //
                    set_number_of_stack( number_of_stack_where_I_locate_card );

                   game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMaximumHeight( down_stack_card_height );
                   game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMinimumHeight( down_stack_card_height );

                   game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( card_stack[ card_stack.size() - 1] );

                   card_stack.pop_back();
                }
                else
                {
                        up_h_layout->setAlignment( card_stack[ card_stack.size() - 1], Qt::AlignHCenter | Qt::AlignTop );
                }

           }
    }
    else
    {
          if( inCard->get_number_of_value() == 12)
          {
              down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number ].addWidget( this );
              game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( card_stack[ card_stack.size() - 1] );

              down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);

              if( get_number_in_stack() == 0 )
              {
                  up_h_layout->insertWidget(1, top_stack_card);
                  up_h_layout->setAlignment(top_stack_card, Qt::AlignHCenter | Qt::AlignTop);
              }
              else
              {
                  up_h_layout->insertWidget(1, card_stack[ card_stack.size() - 2]);
                  up_h_layout->setAlignment(card_stack[ card_stack.size() - 2], Qt::AlignHCenter | Qt::AlignTop);
              }

              set_number_of_stack( number_of_stack_where_I_locate_card );

              card_stack.pop_back();

          }
          else
              up_h_layout->setAlignment( card_stack[ card_stack.size() - 1], Qt::AlignHCenter | Qt::AlignTop );
    }

}

/*********************************************************************************/
void Card_label::locate_card_of_game_vector_in_up_v_layout(
        int number_of_stack_where_I_locate_card,
        int number_of_stack_where_I_take_card,
        Card_label* inCard )
{

    if( final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() > 0)
    {
        if((
            (inCard->get_number_of_value() - 1) == final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ][ final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() - 1]->get_number_of_value() )
            &&
            (inCard->get_mast() == final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ][ final_stack[ number_of_stack_where_I_locate_card - final_stack1_number ].size() - 1]->get_mast() ) )
        {

            // re- creating of card and locating card over cards
            //
            Card_label* temp = new Card_label();
            temp->set_card_parameters( *game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ] );
            delete game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ];
            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].pop_back();

            temp->setMaximumHeight( card_height );
            temp->setMinimumHeight( card_height );
            temp->setPixmap( *temp->get_front_image() );

            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].push_back( temp );

            up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].removeWidget( final_stack[number_of_stack_where_I_locate_card - final_stack1_number][final_stack[number_of_stack_where_I_locate_card - final_stack1_number].size() - 1] );

            final_stack[number_of_stack_where_I_locate_card - final_stack1_number].push_back( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ] );
            temp->set_number_of_stack( number_of_stack_where_I_locate_card);

            up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].addWidget( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ] );

            up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].setAlignment( temp, Qt::AlignHCenter | Qt::AlignTop);

            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].pop_back();

            if( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() > 0)
            {
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number][ game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() - 1]->set_isFrontImage( true );
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() - 1]->setPixmap( *game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() - 1]->get_front_image() );
            }
        }
        else
        {
            if( get_number_in_stack() > 0)
            {
                down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMaximumHeight( down_stack_card_height );
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMinimumHeight( down_stack_card_height );
            }
            else
                down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
        }
    }
    else
    {
        if( inCard->get_number_of_value() == 0)
        {
                final_stack[number_of_stack_where_I_locate_card - final_stack1_number].push_back( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ] );
                set_number_of_stack( number_of_stack_where_I_locate_card);

                up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].removeWidget( final_stack[number_of_stack_where_I_locate_card - final_stack1_number][final_stack[number_of_stack_where_I_locate_card - final_stack1_number].size() - 1]->get_empty_label_of_final_stack() );
                up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].addWidget( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ] );
                up_v_layouts[number_of_stack_where_I_locate_card - final_stack1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 1 ], Qt::AlignHCenter | Qt::AlignTop);

                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].pop_back();

                if( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() > 0)
                {
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number][ game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() - 1]->set_isFrontImage( true );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() - 1]->setPixmap( *game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[ number_of_stack_where_I_take_card - game_vector1_number].size() - 1]->get_front_image() );

                }
        }
        else
        {
            if( get_number_in_stack() > 0)
            {
                down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMaximumHeight( down_stack_card_height );
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMinimumHeight( down_stack_card_height );
            }
            else
                down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);

        }
    }

}
/*********************************************************************************/
void Card_label::locate_card_of_game_vector_in_down_v_layout(
        int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
        int number_of_stack_where_I_locate_card,
        int number_of_stack_where_I_take_card,
        Card_label* inCard )
{

    int number_in_stack = get_number_in_stack();

    if(  number_of_stack_where_I_take_card != number_of_stack_where_I_locate_card )
    {
        if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0)
        {
            if( inCard->get_number_of_value() == ( number_of_value_of_the_last_card_in_layout_where_i_locate_a_card - 1 ) && isRedCard != game_vectors[number_of_stack_where_I_locate_card - game_vector1_number][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->isRedCard)
            {
                    down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].addWidget( this );

                    // where i locate a card_label
                    //
                    if( number_in_stack > 0)
                    {
                        game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->setPixmap( *game_vectors[ number_of_stack - game_vector1_number ][ number_in_stack - 1]->get_front_image() );
                        game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->set_isFrontImage( true );
                    }

                    set_number_of_stack( number_of_stack_where_I_locate_card );

                    if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0 )
                    {
                        game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMaximumHeight( down_stack_card_height );
                        game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMinimumHeight( down_stack_card_height );
                    }

                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ] );

                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].pop_back();
            }
            else
            {
                if( number_in_stack > 0)
                {
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMaximumHeight( down_stack_card_height );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMinimumHeight( down_stack_card_height );
                }
                else
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
            }
        }
        else
        {
            if( inCard->get_number_of_value() == 12)
            {

                if( number_in_stack > 0)
                {
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->setPixmap( *game_vectors[ number_of_stack - game_vector1_number ][ number_in_stack - 1]->get_front_image() );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->set_isFrontImage( true );
                }

                down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].addWidget( this );
                game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ] );

                set_number_of_stack( number_of_stack_where_I_locate_card );

                down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
                game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].pop_back();

            }
            else
            {
                if( number_in_stack > 0)
                {
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMaximumHeight( down_stack_card_height );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMinimumHeight( down_stack_card_height );
                }
                else
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
            }
        }
    }
    else
    {
        if( number_in_stack > 0)
        {
            down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ], Qt::AlignHCenter | Qt::AlignTop);
            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMaximumHeight( down_stack_card_height );
            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_take_card - game_vector1_number].size() - 2 ]->setMinimumHeight( down_stack_card_height );
        }
        else
            down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
    }


}

/*********************************************************************************/
void Card_label::locate_card_of_final_stack_in_down_v_layout(
        int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
        int number_of_stack_where_I_locate_card,
        int number_of_stack_where_I_take_card,
        Card_label* inCard )

{
    if( number_of_stack_where_I_take_card != number_of_stack_where_I_locate_card )
    {       
        int number_in_stack = get_number_in_stack();

        if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0)
        {
            if( inCard->get_number_of_value() == ( number_of_value_of_the_last_card_in_layout_where_i_locate_a_card - 1 ) && isRedCard != game_vectors[number_of_stack_where_I_locate_card - game_vector1_number][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->isRedCard)
            {
                down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].addWidget( this );

                // where i locate a card_label
                //
                if( number_in_stack > 0)
                {
                    up_v_layouts[ number_of_stack_where_I_take_card - final_stack1_number ].addWidget( final_stack[number_of_stack_where_I_take_card - final_stack1_number][number_in_stack - 1] );
                    up_v_layouts[ number_of_stack_where_I_take_card - final_stack1_number ].setAlignment( final_stack[number_of_stack_where_I_take_card - final_stack1_number][number_in_stack - 1], Qt::AlignHCenter | Qt::AlignTop);
                }
                else
                {
                    up_v_layouts[ number_of_stack_where_I_take_card - final_stack1_number ].addWidget( final_stack[number_of_stack_where_I_take_card - final_stack1_number][number_in_stack]->get_empty_label_of_final_stack() );
                    up_v_layouts[ number_of_stack_where_I_take_card - final_stack1_number ].setAlignment( final_stack[number_of_stack_where_I_take_card - final_stack1_number][number_in_stack]->get_empty_label_of_final_stack(), Qt::AlignHCenter | Qt::AlignTop);
                }

                if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0 )
                {
                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMaximumHeight( down_stack_card_height );
                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMinimumHeight( down_stack_card_height );

                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( final_stack[ number_of_stack_where_I_take_card - final_stack1_number ][ number_in_stack ] );

                    down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number ].setAlignment( game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].size() - 1], Qt::AlignHCenter | Qt::AlignTop );
                    final_stack[ number_of_stack_where_I_take_card - final_stack1_number ].pop_back();
                }

                set_number_of_stack( number_of_stack_where_I_locate_card );
            }
            else
            {
                up_v_layouts[number_of_stack_where_I_take_card - final_stack1_number].setAlignment( final_stack[ number_of_stack_where_I_take_card - final_stack1_number][ number_in_stack ], Qt::AlignTop | Qt::AlignCenter );
            }
        }
    }
    else
    {
        up_v_layouts[ number_of_stack_where_I_take_card - final_stack1_number].setAlignment( final_stack[ number_of_stack_where_I_take_card - final_stack1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop );
    }
}

/*********************************************************************************/
void Card_label::locate_group_of_cards_in_down_v_layout(
        int number_of_value_of_the_last_card_in_layout_where_i_locate_a_card,
        int number_of_stack_where_I_locate_card,
        int number_of_stack_where_I_take_card,
        Card_label *inCard)
{
    int number_in_stack = get_number_in_stack();
    int size_of_layout_of_inCard = game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].size() - 1;

    if(  number_of_stack_where_I_take_card != number_of_stack_where_I_locate_card &&
         (number_of_stack_where_I_locate_card >= game_vector1_number && number_of_stack_where_I_locate_card<= game_vector7_number) )
    {
        if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0)
        {
            if( ( inCard->get_number_of_value() + 1 == number_of_value_of_the_last_card_in_layout_where_i_locate_a_card )
                    &&
                (isRedCard != game_vectors[number_of_stack_where_I_locate_card - game_vector1_number][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->isRedCard)
              )
            {
                if( number_in_stack > 0)
                {
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->setPixmap( *game_vectors[ number_of_stack - game_vector1_number ][ number_in_stack - 1]->get_front_image() );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->set_isFrontImage( true );
                }

                if( game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() > 0 )
                {
                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMaximumHeight( down_stack_card_height );
                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ game_vectors[number_of_stack_where_I_locate_card - game_vector1_number].size() - 1 ]->setMinimumHeight( down_stack_card_height );
                }

                for( int i = number_in_stack ; i <= size_of_layout_of_inCard  ; i++)
                {
                   down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].addWidget( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ i ] );
                   game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ i ] );

                   game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ i ]->set_number_of_stack( number_of_stack_where_I_locate_card );

                   down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
                   game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].pop_back();
               }
            }
            else
            {
                if( number_in_stack > 0)
                {
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMaximumHeight( down_stack_card_height );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMinimumHeight( down_stack_card_height );
                }
                else
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
            }
        }
        else
        {
            if( inCard->get_number_of_value() == 12)
            {
                if( number_in_stack > 0)
                {
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->setPixmap( *game_vectors[ number_of_stack - game_vector1_number ][ number_in_stack - 1]->get_front_image() );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1]->set_isFrontImage( true );
                }

                 for( int i = number_in_stack ; i <= size_of_layout_of_inCard  ; i++)
                 {
                    down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].addWidget( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ i ] );
                    game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ].push_back( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ i ] );

                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ i ]->set_number_of_stack( number_of_stack_where_I_locate_card );

                    down_v_layout[ number_of_stack_where_I_locate_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_locate_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ].pop_back();
                }
            }
            else
            {
                if( number_in_stack > 0)
                {
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMaximumHeight( down_stack_card_height );
                    game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMinimumHeight( down_stack_card_height );
                }
                else
                    down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
            }
        }
    }
    else
    {
        if( number_in_stack > 0)
        {
            down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMaximumHeight( down_stack_card_height );
            game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack - 1 ]->setMinimumHeight( down_stack_card_height );
        }
        else
            down_v_layout[ number_of_stack_where_I_take_card - game_vector1_number].setAlignment( game_vectors[ number_of_stack_where_I_take_card - game_vector1_number ][ number_in_stack ], Qt::AlignHCenter | Qt::AlignTop);
    }


}
/**********************************************************************/
//Tests realizations
//
void MainWindow::test_mainwindow_inf()
{
       DO_TEST( main_v_layout      != nullptr) ;
       DO_TEST( down_h_layout      != nullptr) ;
       DO_TEST( image_empty        != nullptr) ;
       DO_TEST( image_back_of_card != nullptr) ;
       DO_TEST( next_card_btn      != nullptr) ;
       DO_TEST( razdelitel         != nullptr) ;
}

/**********************************************************************/
void test_global_inf()
{
     DO_TEST( down_stack_card_height  == 40 );
     DO_TEST( card_width              == 152);
     DO_TEST( card_height             == 220);
     DO_TEST( amount_of_cards_in_deck == 52 );
     DO_TEST( card_deck_number        == 0 );
     DO_TEST( card_stack_number       == 1 );
     DO_TEST( final_stack1_number     == 2 );
     DO_TEST( final_stack2_number     == 3 );
     DO_TEST( final_stack3_number     == 4 );
     DO_TEST( final_stack4_number     == 5 );
     DO_TEST( game_vector1_number     == 6 );
     DO_TEST( game_vector2_number     == 7 );
     DO_TEST( game_vector3_number     == 8 );
     DO_TEST( game_vector4_number     == 9 );
     DO_TEST( game_vector5_number     == 10);
     DO_TEST( game_vector6_number     == 11);
     DO_TEST( game_vector7_number     == 12);
     DO_TEST( size_of_final_stacks    == 13);

     DO_TEST( empty_label1            != nullptr );
     DO_TEST( empty_label2            != nullptr );
     DO_TEST( empty_label3            != nullptr );
     DO_TEST( empty_label4            != nullptr );
     DO_TEST( empty_label5            != nullptr );
     DO_TEST( empty_label6            != nullptr );
     DO_TEST( empty_label7            != nullptr );
     DO_TEST( final_empty_label1      != nullptr );
     DO_TEST( final_empty_label2      != nullptr );
     DO_TEST( final_empty_label3      != nullptr );
     DO_TEST( final_empty_label4      != nullptr );
     DO_TEST( top_stack_card          != nullptr );
}
