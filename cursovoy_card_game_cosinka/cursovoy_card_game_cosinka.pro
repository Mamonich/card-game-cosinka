QT       += core gui
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    MyTests.cpp \
    card.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    MyTests.h \
    card.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    recources.qrc

DISTFILES += \
    resources/10c.png \
    resources/10d.png \
    resources/10h.png \
    resources/10s.png \
    resources/2c.png \
    resources/2d.png \
    resources/2h.png \
    resources/2s.png \
    resources/3c.png \
    resources/3d.png \
    resources/3h.png \
    resources/3s.png \
    resources/4c.png \
    resources/4d.png \
    resources/4h.png \
    resources/4s.png \
    resources/5c.png \
    resources/5d.png \
    resources/5h.png \
    resources/5s.png \
    resources/6c.png \
    resources/6d.png \
    resources/6h.png \
    resources/6s.png \
    resources/7c.png \
    resources/7d.png \
    resources/7h.png \
    resources/7s.png \
    resources/8c.png \
    resources/8d.png \
    resources/8h.png \
    resources/8s.png \
    resources/9c.png \
    resources/9d.png \
    resources/9h.png \
    resources/9s.png \
    resources/Ac.png \
    resources/Ad.png \
    resources/Ah.png \
    resources/As.png \
    resources/Jc.png \
    resources/Jd.png \
    resources/Jh.png \
    resources/Js.png \
    resources/Kc.png \
    resources/Kd.png \
    resources/Kh.png \
    resources/Ks.png \
    resources/Qc.png \
    resources/Qd.png \
    resources/Qh.png \
    resources/Qs.png
