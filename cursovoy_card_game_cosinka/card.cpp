#include "card.h"
#include"MyTests.h"
/////////////////////
// const massives

const QString masts[]
{
  "cherva",
  "buba",
  "pika",
  "krest"
};

const QString values[]
{
    "A",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "V",
    "Q",
    "K"
};

///////////////////
// global parametres
int Card_label_i = 0;
int Card_label_j = 0;

Card* mix_deck( Card inDeck[] )
{
  Card temp;
  int rand_number;

  srand( time(0) );
  for( int i = 0; i < 52; i++)
  {
      rand_number = rand() % 52;

      temp = inDeck[i];
      inDeck[i] = inDeck[ rand_number ];
      inDeck[ rand_number ] = temp;
  }

  return inDeck;
}
// class Card methods
//
/*********************************************************************************/
Card::Card()
{
    if( Card_label_i > 3) // not must create a card after creating of deck of 52 cards
        return;

    mast = masts[Card_label_i];
    value = values[Card_label_j++];

    QString step_way_to_file = ":/resources/" + value + "_" + mast + ".png";

    front_image = new QPixmap( step_way_to_file );
    back_image = new QPixmap( ":/resources/back.png" );

    if( Card_label_i <= 1)
        isRedCard = true;
    else
        isRedCard = false;

    if(Card_label_j == 13)
    {
        Card_label_i++;
        Card_label_j = 0;
    }
}

/*********************************************************************************/
void test_card_inf( Card* deck)
{
    Card_label_i = 0;
    Card_label_j = 0;

    for(int i=0; i < 52; i++)
    {
        DO_TEST( deck[i].get_mast() == masts[Card_label_i]);
        DO_TEST( deck[i].get_value() ==  values[Card_label_j++]);

        if(Card_label_j == 13)
        {
            Card_label_i++;
            Card_label_j = 0;
        }
    }
}
